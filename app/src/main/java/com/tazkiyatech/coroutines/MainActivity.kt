package com.tazkiyatech.coroutines

import android.graphics.Color
import android.os.Bundle
import android.os.SystemClock.sleep
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {

    private val currentThreadName: String
        get() = Thread.currentThread().name

    private var ioJob: Job? = null
    private var countdownJob: Job? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startIOJobButton.setOnClickListener { startIOJob() }
        stopIOJobButton.setOnClickListener { stopIOJob() }

        startCountdownButton.setOnClickListener { startCountdown() }
        stopCountdownButton.setOnClickListener { stopCountdownJob() }
    }

    private fun startIOJob() {
        cancelJobs()

        statusTextView.setBackgroundColor(Color.BLUE)
        statusTextView.setText(R.string.io_job_started)

        ioJob = lifecycleScope.launch(Dispatchers.IO) { executeBackgroundTask() }
    }

    private fun startCountdown() {
        cancelJobs()

        statusTextView.setBackgroundColor(Color.BLUE)
        statusTextView.setText(R.string.countdown_started)

        countdownJob = lifecycleScope.launch(Dispatchers.Main) { performCountdown() }
    }

    private fun stopIOJob() {
        ioJob?.let {
            if (!it.isCompleted) {
                it.cancel()
                statusTextView.setBackgroundColor(Color.RED)
                statusTextView.setText(R.string.io_job_stopped)
            }
        }
    }

    private fun stopCountdownJob() {
        countdownJob?.let {
            if (!it.isCompleted) {
                it.cancel()
                statusTextView.setBackgroundColor(Color.RED)
                statusTextView.setText(R.string.countdown_stopped)
            }
        }
    }

    private fun cancelJobs() {
        ioJob?.cancel()
        countdownJob?.cancel()
    }

    private suspend fun executeBackgroundTask() {
        Log.d("foo", "Started background task in thread $currentThreadName")

        coroutineScope {
            val job1 = launch {
                Log.d("foo", "asynchronous task 1 started in thread $currentThreadName")
                sleep(6000)
                Log.d("foo", "asynchronous task 1 completed in thread $currentThreadName")
            }

            val job2 = launch {
                Log.d("foo", "asynchronous task 2 started in thread $currentThreadName")
                sleep(3000)
                Log.d("foo", "asynchronous task 2 completed in thread $currentThreadName")
            }

            job1.join()
            job2.join()
        }
        
        Log.d("foo", "Completed background task in thread $currentThreadName")

        withContext(Dispatchers.Main) {
            statusTextView.setText(R.string.io_job_completed)
            statusTextView.setBackgroundColor(Color.BLACK)
        }
    }

    private suspend fun performCountdown() {
        Log.d("foo", "Started countdown in thread $currentThreadName")

        for (i in 5 downTo 1) {
            statusTextView.text = getString(R.string.countdown_XX, i)
            delay(1000)
        }

        Log.d("foo", "Completed countdown in thread $currentThreadName")

        statusTextView.setBackgroundColor(Color.BLACK)
        statusTextView.setText(R.string.countdown_completed)
    }
}
